# language: ru


Функция: Создание заведения
  @createPlace
  Сценарий: Успешная Создание заведения
    Допустим я нохожусь на странице Авторизация
    Если я ввожу "John Week" в поле "username"
    И я ввожу "123" в поле "password"
    И нажимаю на кнопку "Login"
    И я вижу оповещение "Logged in successfully!"
    И я вижу имя пользователя "Killer"
    И я выбераю кнопку "Создать новое заведение"
    И я ввожу "Test place" в поле "title"
    И я ввожу описание "Test test test" в поле "description"
    И я загружаю картинку
    И я подтверждаю что согласен с регистрацией
    И я нажимаю на "Create Place"
    То я вижу текст "Test place"