import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";

class PlaceForm extends Component {
    state = {
        title: '',
        image:'',
        description: '',
        isAgree: false,
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    checkBoxHandler = (event) =>{
        this.setState({isAgree: event.target.checked})
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <FormElement
                    propertyName="title"
                    title="Title"
                    type="text" required
                    onChange={this.inputChangeHandler}
                    value={this.state.title}
                />

                <FormElement
                    propertyName="image"
                    title="Image"
                    type="file" required
                    onChange={this.fileChangeHandler}
                />

                <FormElement
                    propertyName="description"
                    title="Description"
                    type="textarea" required
                    onChange={this.inputChangeHandler}
                    value={this.state.description}
                />

                <FormGroup row>
                    <Col sm={{offset: 2, size: 10}}>
                        <FormGroup check inline>
                            <Label check>
                                <Input type="checkbox" required name="isAgree" onChange={this.checkBoxHandler}/>
                                I understand
                            </Label>
                        </FormGroup>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Create Place</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default PlaceForm;