import React from 'react';
import PropTypes from 'prop-types';
import PlaceThumbnail from "../PlaceThumbnail/PlaceThumbnail";
import './PlaceListItem.css';
import {Button, Col, Row} from "reactstrap";
import {Link} from "react-router-dom";
import OverallRating from "../AverageRatings/OverallRating";

const PlaceListItem = props => {

  return (
    <div className='CardBody'>
      <div className='Card'>
        <div className='Img'>
          <Link to={`/places/${props._id}`}>
            <h2>{props.title}</h2>
            <PlaceThumbnail onClick={props.onClick} image={props.image}/>
          </Link>
        </div>
        <div className='ratingAnd'>
          <div>({props && props.countComment ? props.countComment.count:null} Reviews)</div>
          <OverallRating  numOverall={props.numOverall}/>
          <span>Photos:{props.gallery.length}</span>
        </div>
      </div>
        {(props.user && props.user.role === 'admin') ? <div>
            <Row>
                <Col sm={4}>
                    {(props.removed === true) ? <h5 style={{color: 'red', textAlign: 'center'}}>
                        <strong>Удалено!</strong>
                    </h5>: null}
                </Col>
                {props.user &&
                <Col sm={2}>
                    <Button
                        style={{margin: '20px'}}
                        color="danger"
                        className="float-right"
                        onClick={() => props.deletePlace(props._id)}
                    >
                        Remove
                    </Button>
                </Col>
                }
            </Row>
        </div> :null}
    </div>
  );
};

PlaceListItem.propTypes = {
  image: PropTypes.string,
  _id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
};

export default PlaceListItem;
