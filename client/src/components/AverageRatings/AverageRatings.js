import React, {Fragment} from 'react';
import StarRatings from "react-star-ratings";
import {Col, Row} from "reactstrap";
import OverallRating from "./OverallRating";


const AverageRatings = (props) => {
    return (
        <Fragment>
            <hr/>
            <Row>
                <Col sm={2}>
                    Overall
                </Col>
                <Col sm={10}>
                    <OverallRating numOverall={props.numOverall}/>
                </Col>
                <Col sm={2}>
                    Quality of food
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numQuality}
                        ratingToShow={props.numQuality}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numQuality}
                </span>
                </Col>

                <Col sm={2}>
                    Quick service
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numQuick}
                        ratingToShow={props.numQuick}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numQuick}
                </span>
                </Col>
                <Col sm={2}>
                    Interior
                </Col>
                <Col sm={10}>
                    <StarRatings
                        rating={props.numInterior}
                        ratingToShow={props.numInterior}
                        starDimension={'25px'}
                        starRatedColor="orange"
                        numberOfStars={5}
                    />
                    <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {props.numInterior}
                </span>
                </Col>
            </Row>
        </Fragment>


    );
};

export default AverageRatings;