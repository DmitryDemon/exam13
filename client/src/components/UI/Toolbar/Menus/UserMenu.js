import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

const UserMenu = ({user, logout}) => {
    return (
        <UncontrolledDropdown nav inNavbar>
            <DropdownToggle nav caret>
                Hello, {user.username}
            </DropdownToggle>
            <DropdownMenu right>
                <DropdownItem divider/>
                <DropdownItem onClick={logout}>
                    Logout
                </DropdownItem>
                <DropdownItem tag={RouterNavLink} to={"/places/new"} exact>
                    Создать новое заведение
                </DropdownItem>
            </DropdownMenu>
        </UncontrolledDropdown>
        )

};

export default UserMenu;
