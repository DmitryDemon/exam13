import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import PlaceThumbnail from "../../components/PlaceThumbnail/PlaceThumbnail";
import {fetchOnePlace} from "../../store/actions/onePlaceActions";
import Comments from "../Comments/Comments";
import AddComment from "../AddComment/AddComment";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import config from "../../config";
import Slider from "react-slick";
import {createPhoto} from "../../store/actions/placeActions";
import '../../components/PlaceListItem/PlaceListItem.css';
import './OnePlace.css';
import "./SlickCarousel.css";


class onePlace extends Component {
    state={
        gallery: [],
    };


    componentDidMount() {
        this.props.onFetchPlace(this.props.match.params.userId);
    }

    galleryChangeHandler = event => {
        event.preventDefault();

        let files = Array.from(event.target.files);

        files.forEach((file) => {
            let reader = new FileReader();
            reader.onloadend = () => {
                this.setState({
                    gallery: [...this.state.gallery, file],
                });
            };
            reader.readAsDataURL(file);
        });
    };

    submitFormHandler = event => {
        event.preventDefault();
        const formData = new FormData();

        for (let i = 0; i < this.state.gallery.length; i++) {
            formData.append('gallery', this.state.gallery[i]);
        }

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.createPhoto(formData, this.props.match.params.userId);
    };

    render() {
        const permissionAddCommentsAndPictures = this.props.user && this.props.user.role;

        const settings = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1
        };


        return (
            <Fragment>
                {this.props && this.props.onePlace ?
                    <div>
                        <h1>
                            {this.props.onePlace.title}
                        </h1>
                        <div className='img'>
                            <PlaceThumbnail image={this.props.onePlace.image}/>
                            <p>{this.props.onePlace.description}</p>
                            <hr/>
                        </div>

                        <div className='images'>
                            <div className="gallery-slider">
                                <Slider {...settings}>
                                    {
                                        this.props.onePlace.gallery.map(slider => (
                                            <img key={slider} className='Slide-img' src={config.apiURL + '/uploads/' + slider} alt={this.props.onePlace.name}/>
                                        ))
                                    }
                                </Slider>
                            </div>
                        </div>

                        <div className='comments'>
                            <Comments placeId={this.props.match.params.userId}/>
                            {permissionAddCommentsAndPictures ?
                                <div>
                                    <AddComment placeId={this.props.match.params.userId}/>
                                    <Form onSubmit={this.submitFormHandler}>
                                        <FormGroup row>
                                            <Label for="gallery" sm={2}>Галерея</Label>
                                            <Col sm={10}>
                                                <Input
                                                    type="file"
                                                    name="gallery" id="gallery"
                                                    onChange={this.galleryChangeHandler}
                                                    multiple
                                                />
                                            </Col>
                                        </FormGroup>

                                        <FormGroup row>
                                            <Col sm={{offset:2, size: 10}}>
                                                <Button type="submit" color="primary">Add Photo</Button>
                                            </Col>
                                        </FormGroup>
                                    </Form>
                                </div>

                                :null
                            }


                        </div>

                    </div>

                    : <div>Loading</div>
                }
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    onePlace: state.onePlace.onePlace,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    onFetchPlace: id => dispatch(fetchOnePlace(id)),
    createPhoto: (placeData, id) => dispatch(createPhoto(placeData, id))

});

export default connect(mapStateToProps, mapDispatchToProps)(onePlace);
