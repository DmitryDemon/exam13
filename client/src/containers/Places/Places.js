import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import 'moment/locale/ru';
import {Col, Row} from "reactstrap";
import PlaceListItem from "../../components/PlaceListItem/PlaceListItem";
import {deletePlace, fetchPlasce} from "../../store/actions/placeActions";






class Places extends Component {
    componentDidMount() {
        this.props.fetchPlasce(this.props.match.params.userId)
    };

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.userId !== this.props.match.params.userId) {
            this.props.fetchPlasce(this.props.match.params.userId);
        }
    };
    render() {
        return (
            <Fragment>
                <h1>Top page</h1>
                <Row>
                    <Col sm={12}>
                        {this.props.places.map((place,key) => {
                            return (
                                    <Fragment key={key}>
                                        <PlaceListItem
                                            key={place._id}
                                            _id={place._id}
                                            title={place.title}
                                            image={place.image}
                                            description={place.description}
                                            removed={place.removed}
                                            userRecipe={place.user}
                                            user={this.props.user}
                                            deletePlace={this.props.deletePlace}
                                            numOverall={place.numOverall}
                                            countComment={place.countComment}
                                            gallery={place.gallery}
                                        />
                                    </Fragment>
                            )
                        })}
                    </Col>
                </Row>
            </Fragment>

        );
    }
}

const mapStateToProps = state => ({
    places: state.places.places,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchPlasce: userId => dispatch(fetchPlasce(userId)),
    deletePlace: (id) => dispatch(deletePlace(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Places);
