import React, {Component, Fragment} from 'react';
import StarRatings from "react-star-ratings";

import {connect} from "react-redux";
import {Button, Card, CardText, CardTitle, Col, Row} from "reactstrap";
import {deleteComment, fetchComment} from "../../store/actions/commentActions";
import AverageRatings from "../../components/AverageRatings/AverageRatings";
import axios from "../../axios-api";


class Comments extends Component {

    componentDidMount() {
        this.props.fetchComments(this.props.placeId)
    }

    putOveral = (numOverall,placeId) => {
        axios.patch(`/places`,{
            numOverall,
            placeId
        })
    };

    render() {

        // console.log(this.props.comments);
        let numOverall = 0;
        let numQuality = 0;
        let numQuick = 0;
        let numInterior = 0;

        this.props.comments.map(comment => {
            numQuality += comment.ratingQualityOfFood / this.props.comments.length;
            numQuick += comment.ratingQuickService / this.props.comments.length;
            numInterior += comment.ratingInterior / this.props.comments.length;
            numOverall = (numQuality + numQuick + numInterior) / 3;

        });
        this.putOveral(numOverall,this.props.placeId);
        return (
            <Fragment>
                <AverageRatings
                    numOverall={parseFloat(numOverall.toFixed(1))}
                    numQuality={parseFloat(numQuality.toFixed(1))}
                    numQuick={parseFloat(numQuick.toFixed(1))}
                    numInterior={parseFloat(numInterior.toFixed(1))}
                />
                <h3>Comments</h3>
                {this.props.comments.length > 0 ? this.props.comments.map(comment => (
                    <Card key={comment._id} className="mt-2 p-3">
                        <CardTitle>
                            On {comment.dateTime}, <span>{comment.user.username}:</span>
                        </CardTitle>
                        <CardText>{comment.commentText}</CardText>

                        {(this.props.user._id === comment.user._id) && this.props.user.role === 'admin' ?
                            <Col sm={2}>
                                <Button
                                    style={{margin: '20px'}}
                                    color="danger"
                                    className="float-right"
                                    onClick={() => this.props.deleteComment(comment._id,comment)}
                                    >
                                    Remove
                                </Button>
                        </Col>
                            :null}


                        <Row>
                            <Col sm={2}>
                                Quality of food:
                            </Col>

                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.ratingQualityOfFood}
                                    ratingToShow={comment.ratingQualityOfFood}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ratingQualityOfFood}
                             </span>
                            </Col>
                            <Col sm={2}>
                                Quick service:
                            </Col>

                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.ratingQuickService}
                                    ratingToShow={comment.ratingQuickService}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ratingQuickService}
                             </span>
                            </Col>
                            <Col sm={2}>
                                Interior:
                            </Col>
                            <Col sm={10}>
                                <StarRatings
                                    rating={comment.ratingInterior}
                                    ratingToShow={comment.ratingInterior}
                                    starDimension={'25px'}
                                    starRatedColor="orange"
                                    numberOfStars={5}
                                />

                                <span style={{display: 'inline-block', marginLeft: '20px', verticalAlign: 'text-top'}}>
                                 {comment.ratingInterior}
                             </span>
                            </Col>
                        </Row>
                    </Card>
                )): <div>No Comments</div>}

            </Fragment>

        );
    }
}


const mapStateToProps = state => ({
    comments: state.comments.comments,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchComments: placeId => dispatch(fetchComment(placeId)),
    deleteComment: (id,comment) => dispatch(deleteComment(id,comment)),

});

export default connect(mapStateToProps, mapDispatchToProps)(Comments);