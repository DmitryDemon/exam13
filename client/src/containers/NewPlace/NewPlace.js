import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import PlaceForm from "../../components/PlaceForm/PlaceForm";
import {createPlace} from "../../store/actions/placeActions";



class NewPlace extends Component {

    createPlace = placeData => {
        this.props.onPlaceCreated(placeData).then(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New Recipe</h2>
                <PlaceForm
                    onSubmit={this.createPlace}
                    places={this.props.places}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    places: state.places.places,
});

const mapDispatchToProps = dispatch => ({
    onPlaceCreated: placeData => dispatch(createPlace(placeData)),

});

export default connect(mapStateToProps, mapDispatchToProps)(NewPlace);