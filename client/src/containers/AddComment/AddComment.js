import React, {Component} from 'react';
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import {Button, Col, Form, Input, Label, Row} from "reactstrap";
import {createComment} from "../../store/actions/commentActions";

const NUMBER = ['5.0','4.9', '4.8', '4.7', '4.6', '4.5', '4.4', '4.3','4.2','4.1', '4.0',
                '3.9', '3.8', '3.7', '3.6', '3.5', '3.4', '3.3','3.2','3.1', '3.0',
                '2.9', '2.8', '2.7', '2.6', '2.5', '2.4', '2.3','2.2','2.1', '2.0',
                '1.9', '1.8', '1.7', '1.6', '1.5', '1.4', '1.3','1.2','1.1', '1.0',];

class AddComment extends Component {
    state = {
        commentText: '',
        ratingQualityOfFood: '',
        ratingQuickService: '',
        ratingInterior: '',
        placeId: this.props.placeId
    };


    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitFormHandler = event => {
        event.preventDefault();

        this.props.createComment({...this.state})

    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <hr/>
                <FormElement
                    type="textarea"
                    propertyName="commentText"
                    title="Add a comment:"
                    onChange={this.inputChangeHandler}
                />

                <Row>
                    <Col sm={3}>
                        <Label>Quality of food</Label>
                        <Input
                            type="select"
                            name="ratingQualityOfFood"
                            value={this.state.ratingQualityOfFood}

                            onChange={this.inputChangeHandler}

                        >
                            {NUMBER.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3}>
                        <Label>Quick service</Label>
                        <Input
                            type="select"
                            name="ratingQuickService"
                            value={this.state.ratingQuickService}
                            onChange={this.inputChangeHandler}
                        >
                            {NUMBER.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3}>
                        <Label>Interior</Label>

                        <Input
                            type="select"
                            name="ratingInterior"
                            onChange={this.inputChangeHandler}
                            value={this.state.ratingInterior}

                        >
                            {NUMBER.map(number => (
                                <option key={number} value={number}>{number}</option>
                            ))}
                        </Input>
                    </Col>
                    <Col sm={3} className="p-4 mt-1">
                        <Button type="submit" color="primary">
                            Add
                        </Button>
                    </Col>
                </Row>
            </Form>
        );
    }
}
const mapDispatchToProps = dispatch =>({
    createComment: data => dispatch(createComment(data)),
});

export default connect(null,mapDispatchToProps)(AddComment);