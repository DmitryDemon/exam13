import {FETCH_PLACE_SUCCESS} from "../actions/placeActions";
import {FETCH_ONE_PLACE_SUCCESS} from "../actions/onePlaceActions";


const initialState = {
  places: [],
  onePlace: null,
};

const placesReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_PLACE_SUCCESS:
      return {...state, places: action.places};
    case FETCH_ONE_PLACE_SUCCESS:
      return {...state, onePlace: action.places};
    default:
      return state;
  }
};

export default placesReducer;
