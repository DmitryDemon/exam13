import axios from '../../axios-api';
import {push} from "connected-react-router";

export const FETCH_PLACE_SUCCESS = 'FETCH_PLACE_SUCCESS';
export const CREATE_PLACE_SUCCESS = 'CREATE_PLACE_SUCCESS';

export const fetchPlacesSuccess = places => ({type: FETCH_PLACE_SUCCESS, places});
export const createPlaceSuccess = () => ({type: CREATE_PLACE_SUCCESS});



export const fetchPlasce = userId => {
    return dispatch => {
        let url = '/places';
        if (userId) {
            url += '?user=' + userId
        }
        return axios.get(url).then(
            response => dispatch(fetchPlacesSuccess(response.data))
        );
    };
};

export const createPlace = placeData => {
    return dispatch => {
        return axios.post('/places', placeData).then(
            () => dispatch(createPlaceSuccess())
        );
    };
};

export const createPhoto = (placeData, id, redirect = true) => {
    return dispatch => {
        return axios.post(`/places/${id}`, placeData).then(
            response => {
                dispatch(createPlaceSuccess(response.data));
                if (redirect) {
                    dispatch(push('/places/' + response.data._id));
                }
            },
            error => {

            }
        )
    }
};

export const deletePlace = id => {
    return (dispatch) => {
        return axios.delete(`/places/${id}`).then(
            () => {
                dispatch(fetchPlasce())
            }
        )
    }
};







