import axios from '../../axios-api';

export const FETCH_ONE_PLACE_SUCCESS = 'FETCH_ONE_PLACE_SUCCESS';


export const fetchOnePlaceSuccess = places => ({type: FETCH_ONE_PLACE_SUCCESS, places});



export const fetchOnePlace = id => {
    return (dispatch) => {
        return axios.get(`/places/${id}`).then(
            response => {
                dispatch(fetchOnePlaceSuccess(response.data));
            }
        );
    };
};