import React from 'react';
import {Route, Switch} from "react-router-dom";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Places from "./containers/Places/Places";
import NewPlace from "./containers/NewPlace/NewPlace";
import OnePlace from "./containers/OnePlace/OnePlace";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Places}/>
      <Route path="/places/new" exact component={NewPlace}/>
      <Route path="/places/:userId" exact component={OnePlace}/>
      <Route path="/register" exact component={Register}/>
      <Route path="/login" exact component={Login}/>
    </Switch>
  );
};

export default Routes;