const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Comment = require('../models/Comment');


const router = express.Router();


router.get('/', async (req, res) => {

    const criteria = {};

    if (req.query.placeId) {
        criteria.placeId = req.query.placeId
    }

    Comment.find(criteria).populate('user', 'username')
        .then(result => res.send(result))
        .catch((error) => res.status(500).send(error))
});


router.post('/', auth, async (req, res) => {
    try {
        const commentData = req.body;

        const comment = await Comment(commentData);

        comment.dateTime = new Date().toISOString();
        comment.user = req.user._id;

        await comment.save();

        return res.send(comment)
    } catch (e) {
        return res.status(500)
    }
});

router.delete('/:id', auth,async (req,res)=>{
    console.log(req.params);
    try {

        await Comment.findByIdAndDelete(req.params.id);
            res.send('ok');

    }catch (e) {
        return res.status(500)
    }
});


module.exports = router;