const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const permit = require('../middleware/permit');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const Place = require('../models/Place');
const Comment = require('../models/Comment');


const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {

    try {
        let criteria = {removed: false};

        if (req.query.user) {
            criteria.user = req.query.user;
        }

        const place = await Place.find(criteria).populate('user', '_id, displayName');

        const commentsCount = await Comment.aggregate([{$group: {_id: '$placeId', count: {$sum: 1}}}]);//

        const placesWithCount = place.map(place => {
            const placeCount = commentsCount.find(pc => pc._id.equals(place._id));
            return {
                ...place.toObject(),
                countComment: placeCount
            }
        });

        return res.send(placesWithCount)
    }catch (e) {
        return res.status(500).send(e)
    }

});





router.post('/', auth, upload.single('image'), (req, res) => {

    const placeData = req.body;

    console.log(placeData.isAgree === 'false');


    if (placeData.isAgree === 'false') {
        res.sendStatus(418)
    }else {
        if (req.file) {
            placeData.image = req.file.filename;
        }


        placeData.user = req.user._id;

        const place = new Place(placeData);

        place.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    }
});

router.get('/:id',tryAuth, async (req, res) => {
    try {
        const place = await Place.findById(req.params.id);
        res.send(place);
    }catch (e) {
        res.sendStatus(500);
    }
});

router.post('/:id', [auth, upload.fields([{name: 'gallery', maxCount: 10}])], async (req, res) => {
    try {
        const placeData = await Place.findById(req.params.id);

        if (req.files) {
            placeData.gallery = req.files.gallery ? req.files.gallery.map(file =>  file.filename) : undefined;
        }

        placeData.user = req.user._id;

        const place = new Place(placeData);
        await place.save();

        return res.send(place);
    } catch (e) {
        console.log(e);
        return res.sendStatus(500);
    }
});

router.patch('/', auth, async (req, res) => {
    try {

        const place = await Place.findById(req.body.placeId);

        place.numOverall = req.body.numOverall.toFixed(1);

        await place.save();
        return res.send('ok')
    } catch (e) {
        return res.status(500)
    }

});

router.delete('/:id', [auth, permit('admin')],async (req,res)=>{
    try {
        await Place.findByIdAndDelete({_id: req.params.id});
            res.send('ok');

        await Comment.findByIdAndDelete({userId:req.params.id})
            res.send('ok');

    }catch (e) {
        return res.status(500)
    }
});



module.exports = router;