const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const CommentSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    placeId: {
        type: Schema.Types.ObjectId,
        ref: 'Recipe',
        required: true,
    },
    commentText: {
        type: String,
        required:true,
    },
    dateTime: String,
    ratingQualityOfFood: Number,
    ratingQuickService: Number,
    ratingInterior: Number,


});

const Comment = mongoose.model('Comment', CommentSchema);

module.exports = Comment;
