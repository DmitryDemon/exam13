const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const PlaceSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    image: {
        type: String,
        required:true,
    },
    description: String,
    removed: {
        type: Boolean,
        required: true,
        default: false
    },
    numOverall: Number,
    gallery: [String],
});

const Place = mongoose.model('Place', PlaceSchema);

module.exports = Place;