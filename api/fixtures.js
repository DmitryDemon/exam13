const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Place = require('./models/Place');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const user = await User.create(
        {
            username: 'Haizenberg',
            password: '123',
            token: nanoid(),
            role: 'admin',
            displayName: 'Wolter'
        },
        {
            username: 'John Week',
            password: '123',
            token: nanoid(),
            displayName: 'Killer',
        },
        {
            username: 'Presenter Meow',
            password: '123',
            token: nanoid(),
            displayName: 'Presenter',
        },
    );

    await Place.create(
        {
            user: user[0]._id,
            title: 'McDonald’s',
            image: 'mac.jpeg',
            description: 'Сеть с 75-летней историей, не требующая представления. ' +
                'В России с 1990 года, очереди в первый McDonalds — один из самых ' +
                'запомнившихся моментов в истории новой России. В последние годы всерьез' +
                ' взялись за развитие концепции — завтраки круглые сутки, собранные под' +
                ' заказ бургеры, цифровые инновации, введение элементов free-flow — все для ' +
                'победы в борьбе с новыми fast casual игроками и удержания гостей.'
        },
        {
            user: user[1]._id,
            title: 'Subway',
            image: 'subway.jpg',
            description: 'Самая крупная по числу точек сеть. Позиционируется как здоровая альтернатива' +
                ' стандартным сетям фастфуда, что отражается в слогане компании «Ешь свежее!» (Eat fresh!).' +
                ' Франшиза Subway очень популярна среди начинающих российских рестораторов. Но такая ' +
                'популярность имеет и обратную сторону — неопытные франчайзи частенько портят имидж: стремительно' +
                ' разоряются и обвиняют во всем американцев.'
        },
        {
            user: user[1]._id,
            title: 'Burger King',
            image: 'burger-king.jpg',
            description: 'Американская ли это сеть или уже бразильская – вопрос открытый: ' +
                'с 2010 года контрольный пакет принадлежит компании 3G Capital из Бразилии. ' +
                'В том же году Burger King вышел и на российский рынок'
        },
        {
            user: user[2]._id,
            title: 'Taco Bell',
            image: 'tako.jpg',
            description: 'Международная сеть адаптированной текс-мекс кухни. Любопытно, что на' +
                ' мексиканский рынок Taco Bell пытался выйти дважды, но оба раза безуспешно – местному ' +
                'населению не по нраву адаптации'
        },

    );

    return connection.close();
};


run().catch(error => {
    console.error('Something wrong happened...', error);
});
